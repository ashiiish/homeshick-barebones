# Configure pinentry to use the correct TTY
GPG_TTY=$(tty)
export GPG_TTY
if command -v gpg-connect-agent >/dev/null 2>&1; then
    cmd=gpg-connect-agent
else
    cmd=${HOME}/usr/local/bin/gpg-connect-agent
fi
# ${cmd} updatestartuptty /bye >/dev/null
