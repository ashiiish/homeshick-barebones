#!/usr/bin/env bash

export SHELL_TYPE=bash

# {{{ homeshick setup

source $HOME/.homesick/repos/homeshick/homeshick.sh

hs-clone() {
    repo=${1}
    if [[ ! -z ${repo} ]]; then
        homeshick clone git@gitlab.com:ashiiish/homeshick-${repo}.git
    fi
}

#}}}

# {{{ bash settings

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend
# append and reload the history after each command
PROMPT_COMMAND="history -a; history -n"

# ignore certain commands from the history
HISTIGNORE="ls:ll:cd:pwd:bg:fg:history"

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=100000
HISTFILESIZE=10000000

# }}}

[[ -f "${PERSONAL_LOCAL}/etc/bashrc" ]] && source "${PERSONAL_LOCAL}/etc/bashrc"

if [[ $(uname -s) != Darwin ]]; then
# {{{ load personal configs

SKIP_IMPORT=1 source $HOME/bin/import.sh
source ${HOME}/imports/exports.sh
source ${HOME}/imports/aliases.sh

if [[ -f ${HOME}/bash-locals.sh ]]; then
    source ${HOME}/bash-locals.sh
fi
# }}}

# {{{ guix

export GUIX_PROFILE=${HOME}/.guix-profile
export GUIX_LOCPATH=$HOME/.guix-profile/lib/locale
export GUIX_EXTRA_PROFILES=${HOME}/.guix-extra-profiles
export GUIX_EXTRA=${HOME}/.guix-extra
export GUIX_CURRENT=${HOME}/.config/guix/current
export GUIX_SYSTEM_CURRENT=/run/current-system/profile
[[ -f ${GUIX_PROFILE}/etc/profile ]] && source ${GUIX_PROFILE}/etc/profile
export PATH=/run/setuid-programs:${GUIX_CURRENT}/bin:${GUIX_SYSTEM_CURRENT}/bin:${GUIX_SYSTEM_CURRENT}/sbin:${PATH}

# }}}

# {{{ Pre/PostCommand .envrc load/unload

# This will run before any command is executed.
function PreCommand() {
  if [ -z "$AT_PROMPT" ]; then
    return
  fi
  unset AT_PROMPT

  # # Do stuff.
  # echo "Running PreCommand: $@ ${PWD}"

  # if [[ "${1}" =~ ^cd.* && -f ${TMPDIR}/envrc-${PWD//\//-} ]]; then
  #   source ${TMPDIR}/envrc-${PWD//\//-}
  #   sed 's| | -|g;s| -$||' ${TMPDIR}/envrc-${PWD//\//-}
  #   rm ${TMPDIR}/envrc-${PWD//\//-}
  # fi
}
trap 'PreCommand $BASH_COMMAND' DEBUG

# This will run after the execution of the previous full command line.  We don't
# want PostCommand to execute when first starting a bash session (i.e., at
# the first prompt).
FIRST_PROMPT=1
function PostCommand() {
  AT_PROMPT=1

  if [ -n "$FIRST_PROMPT" ]; then
    unset FIRST_PROMPT
    return
  fi

  # # Do stuff.
  # echo "Running PostCommand: $@ ${PWD}"

  # if [[ -f .envrc && ! -f ${TMPDIR}/envrc-${PWD//\//-} ]]; then
  #   source .envrc
  #   echo "unset $(grep export .envrc | sed -E "s|export (.*)=.*|\1|" | tr '\n' ' ')" > ${TMPDIR}/envrc-${PWD//\//-}
  #   sed 's|unset|export:|;s| | +|g;s| +$||' ${TMPDIR}/envrc-${PWD//\//-}
  # fi

  PS1="$(guile ~/bin/prompt.scm bash 0 left $COLUMNS)"
}
PROMPT_COMMAND="PostCommand"

# }}}

# {{{ PROMPT

# # Create a string like:  "[ Apr 25 16:06 ]" with time in RED.
# printf -v PS1RHS "\e[0m[ \e[0;1;31m%(%b %d %H:%M)T \e[0m]" -1 # -1 is current time
#
# # Strip ANSI commands before counting length
# # From: https://www.commandlinefu.com/commands/view/12043/remove-color-special-escape-ansi-codes-from-text-with-sed
# PS1RHS_stripped=$(sed "s,\x1B\[[0-9;]*[a-zA-Z],,g" <<<"$PS1RHS")
#
# # Reference: https://en.wikipedia.org/wiki/ANSI_escape_code
# local Save='\e[s' # Save cursor position
# local Rest='\e[u' # Restore cursor to save point
#
# # Save cursor position, jump to right hand edge, then go left N columns where
# # N is the length of the printable RHS string. Print the RHS string, then
# # return to the saved position and print the LHS prompt.
#
# # Note: "\[" and "\]" are used so that bash can calculate the number of
# # printed characters so that the prompt doesn't do strange things when
# # editing the entered text.
#
# PS1="\[${Save}\e[${COLUMNS:-$(tput cols)}C\e[${#PS1RHS_stripped}D${PS1RHS}${Rest}\]${PS1}"

# }}}
fi

export PATH=${HOME}/bin:${PATH}
alias dcm="docker-compose"
